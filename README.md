# Projekt na pracownie C - Gra z użycie GTK+ - Kulki

*Pierwszy semestr studiów - zimowy 2014/2015.*

### O kulkach:
http://pl.wikipedia.org/wiki/Kulki_(gra_komputerowa)

### Wykonawca:
Piotr Szymajda - BożydarCompany Game Studio™

### Podziękowania 
za wsparcie dla:

- "Cielak"
- "Dybiec"
- "Kajo"

oraz Alfa-testerom:

- "Asdamos"
- "Kajo"
- "Nati"
- "Zalesia"