#ifndef GLOBAL_H
#define GLOBAL_H

#include <gtk/gtk.h>

#include <stdbool.h>
#include <stdlib.h>

#define ILOSC_NAPISOW 61
#define ILE_ZNIKA 5
#define ILE_NA_START 7
#define LEJ_PO_BOMBIE 3

#define MAX_PATH 250
#define KOLOR_TEM "#660000"

GtkWindow *okno;
GdkPixbuf *ikonka, *konami, *logoBCGS;
GdkPixbuf *kulki[10];
GdkPixbufAnimation * boom, *tutorial;

enum{
    KULKI = 0,
    INFO = 7,
    BLAD = 8,
    WYBIERZ = 14,
    TWOJ_WYNIK = 16,
    REKORD = 17,
    NASTKULK = 18,
    POZIOM = 20,
    WCZYTAJ = 29,
    ZAPISZ = 30,
    POMOC = 31,
    ZASADY = 32,
    GRA = 34,
    SZYBKOSC = 35,
    OSZUKANYRANKING = 42,
    LOSOWYRANK = 43,
    KLIKNIJ = 47,
    PRZEGRALES = 48,
    PLIK = 49,
    CZYNOWA = 50
};

enum kolor{
    BRAK = 0,
    NIEBIESKI,
    ZIELONY,
    CZERWONY,
    SZARY,
    CZARNY,
    ZOLTY,
    ROZOWY,
    BRAZOWY
};
enum kierunek{
    GORA = 100,
    DOL,
    LEWO,
    PRAWO
};

const int punktacja[10];
const int liczbyPierwsze[10];

bool wskazowki, pelenEkran, konamiEvent;
int poziom, szybkosc, klik, konamP;

char jezyk[10];
char napisy[ILOSC_NAPISOW][100];

const gchar wersja[20];
const gchar copyright[50];
const gchar *autor[40];
const gchar *specialni[20];
const gchar *betaTesterzy[20];

#endif // GLOBAL_H
