#include <gtk/gtk.h>

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "GUI/okienka.h"
#include "Funkcje/funkcje_conf.h"
#include "Funkcje/funkcje.h"
#include "GUI/gra.h"

#include "globalne.h"

static void (*funkcje[6])(GtkWidget *widget, gpointer cos) = {graj, Wyniki, Ustawienia, WczytajPlik, Autor, CzyKoniec};

int main(int argc,char *argv[])
{
    gtk_init (&argc, &argv);

    EkranStartowy();

    okno = (GtkWindow*)gtk_window_new(GTK_WINDOW_TOPLEVEL);

    ustawOkno(okno, "Gra: Kulki by Bożydar", FALSE);

    gtk_window_set_default_size(GTK_WINDOW(okno), 900, 600);

    g_signal_connect(G_OBJECT(okno), "delete_event", G_CALLBACK (CzyKoniec), NULL);
    g_signal_connect(G_OBJECT(okno), "destroy" , G_CALLBACK(gtk_main_quit), NULL);
///---------------------------------------------------------------------------
    gtk_container_set_border_width(GTK_CONTAINER(okno), 20);

    GtkBox *menu = (GtkBox*)gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_set_homogeneous(GTK_BOX(menu), FALSE);
    gtk_container_add(GTK_CONTAINER(okno), (GtkWidget*)menu);

    GtkBox *gora = (GtkBox*)gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_set_homogeneous(GTK_BOX(gora), FALSE);
    gtk_container_add(GTK_CONTAINER(menu), (GtkWidget*)gora);

    GtkLabel *tytul = (GtkLabel*)gtk_label_new(napisy[KULKI]);
    ustawNapis(tytul, napisy[KULKI], "120", "blue", "Bauhaus 93");
///---------------------------------------------------------------------------
    GError *blad = NULL;
    GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file_at_size ("./image/kulki.png", 350, 350, &blad);

    wypiszBlad(pixbuf, blad);

    GtkImage *image = (GtkImage*) gtk_image_new_from_pixbuf(pixbuf);
    g_object_unref(pixbuf);
///--------------------------------------------------------------------------- NIESPODZIANKA ;)
    klik = 7;
    GtkWidget * event_box = gtk_event_box_new();

    gtk_container_add(GTK_CONTAINER(event_box), (GtkWidget*)image);

    g_signal_connect(G_OBJECT(event_box), "button_press_event", G_CALLBACK(naciskN), tytul);
///---------------------------------------------------------------------------
    gtk_box_pack_start(GTK_BOX(gora), (GtkWidget*)tytul, TRUE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(gora), (GtkWidget*)event_box, TRUE, FALSE, 0);
///---------------------------------------------------------------------------
    GtkBox *dol = (GtkBox*)gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_set_homogeneous(GTK_BOX(dol), TRUE);
    gtk_container_add(GTK_CONTAINER(menu), (GtkWidget*)dol);

    GtkGrid *przyciski = (GtkGrid*)gtk_grid_new();
    gtk_grid_set_column_spacing(przyciski, 450);
    gtk_grid_set_row_spacing(przyciski, 100);

    GtkButton *button;

    for (int kolumna = 0; kolumna < 2; kolumna++)
    {
        for (int wiersz = 0; wiersz < 3; wiersz++)
        {
            GtkLabel *etykieta = (GtkLabel*)gtk_label_new("");
            ustawNapis(etykieta, napisy[kolumna*3+wiersz+1], "20", "black", "Times New Roman");

            button = (GtkButton*)gtk_button_new();
            gtk_container_add(GTK_CONTAINER(button), GTK_WIDGET(etykieta));

            g_signal_connect(G_OBJECT(button), "clicked", G_CALLBACK(funkcje[kolumna*3+wiersz]), (gpointer)NULL);
            gtk_grid_attach (GTK_GRID (przyciski), (GtkWidget*)button, kolumna, wiersz, 2, 2);
        }
    }

    gtk_box_pack_start(GTK_BOX(dol), (GtkWidget*)przyciski, FALSE, FALSE, 0);

    gtk_main();

return 0;
}
