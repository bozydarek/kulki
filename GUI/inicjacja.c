#include "inicjacja.h"

void wczytajGlobale()
{

    GError *blad = NULL;
    ikonka = gdk_pixbuf_new_from_file_at_scale("./image/kulki.png", 64,64, TRUE, &blad);
    wypiszBlad(ikonka, blad);

    wczytajConf();

    blad = NULL;
    logoBCGS = gdk_pixbuf_new_from_file_at_scale("./image/BCGS_LOGO.png", 400,400, TRUE, &blad);
    wypiszBlad(logoBCGS, blad);

    blad = NULL;
    tutorial = gdk_pixbuf_animation_new_from_file("./image/Tutorial/tut1.gif", &blad);
    wypiszBlad(logoBCGS, blad);


    blad = NULL;
    konami = gdk_pixbuf_new_from_file_at_scale("./image/konamiV.png", 75, 800, TRUE, &blad);
    wypiszBlad(konami, blad);

    blad = NULL;
    boom = gdk_pixbuf_animation_new_from_file("./image/boom.gif", &blad);
    wypiszBlad(boom, blad);

    konamiEvent = false;
    ///---------------------------------------------------------------------------
    kulki[BRAK] = gdk_pixbuf_new_from_file_at_size("./image/Brak.png", 70, 70, &blad);
    for(int i = 1; i<9; i++)
    {
        char sciezka[30];
        sprintf(sciezka, "./image/Standard/kul%d.png", i);
        kulki[i] = gdk_pixbuf_new_from_file_at_size(sciezka, 70, 70, &blad);
        wypiszBlad(kulki[i], blad);
    }
    /// Gotowe ///
    g_print("Wczytywanie Zakonczone\n");
}

static gboolean przesunpasek(gpointer data)
{
    GtkProgressBar * pasek = data;
    if(data == NULL || !GTK_IS_PROGRESS_BAR(pasek))
        return FALSE;
    else if(gtk_progress_bar_get_fraction(pasek) == 1)
        return FALSE;
    else
        gtk_progress_bar_set_fraction(pasek, gtk_progress_bar_get_fraction(pasek)+0.1);
    return TRUE;
}
static gboolean zmienTekst(gpointer data)
{
    GtkLabel * kliknij = data;
    if(strlen(napisy[KLIKNIJ])>=3)
        ustawNapis(kliknij, napisy[KLIKNIJ],"20","#C9D1D1", "Arial");
    else
        ustawNapis(kliknij, "Kliknij na mnie! Click on me!","20","#C9D1D1", "Arial");
    return FALSE;
}
void EkranStartowy()
{
    GtkWindow * EkrStart = (GtkWindow *)gtk_window_new(GTK_WINDOW_TOPLEVEL);
    ustawOkno(EkrStart,"",false);

    gtk_widget_set_size_request (GTK_WIDGET(EkrStart), 1000, 350);
    gtk_window_set_resizable(EkrStart, FALSE);

    GtkProgressBar * pasek = (GtkProgressBar *)gtk_progress_bar_new();

    gtk_widget_set_events (GTK_WIDGET(EkrStart), GDK_BUTTON_PRESS_MASK );
    g_signal_connect (G_OBJECT (EkrStart), "button-press-event", G_CALLBACK (rozpocznj), pasek);
///---------------------------------------------------------------------------------------------------
    GtkLabel * Tytul = (GtkLabel *)gtk_label_new(""), *byME= (GtkLabel *)gtk_label_new(""), *kliknij = (GtkLabel *)gtk_label_new("");
    ustawNapis(Tytul,"K U L K I","105","#C9D1D1","Bauhaus 93");
    ustawNapis(kliknij, "Proszę czekać trwa wczytywanie","20","#C9D1D1", "Arial");
    ustawNapis(byME,(char*)copyright,"20","#C9D1D1","Arial");

    GtkBox * wszystko = (GtkBox *)gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

    gtk_box_pack_start(wszystko, GTK_WIDGET (Tytul), TRUE, TRUE, 10);
    gtk_box_pack_start(wszystko, GTK_WIDGET (byME), TRUE, TRUE, 0);
    gtk_box_pack_start(wszystko, GTK_WIDGET (pasek), TRUE, TRUE, 10);
    gtk_box_pack_start(wszystko, GTK_WIDGET (kliknij), TRUE, TRUE, 40);
///---------------------------------------------------------------------------------------------------
    GtkLayout *layout = (GtkLayout *)gtk_layout_new(NULL, NULL);
    GError *blad = NULL;
    GdkPixbuf *drewno = (GdkPixbuf *)gdk_pixbuf_new_from_file("./image/drewno.jpg", &blad);
    GtkImage *image = (GtkImage *)gtk_image_new_from_pixbuf(drewno);

    if(blad == NULL)
    {
        gtk_container_add(GTK_CONTAINER (EkrStart), GTK_WIDGET (layout));
        gtk_widget_show(GTK_WIDGET (layout));
        gtk_layout_put(layout, GTK_WIDGET (image), 0, 0);
        gtk_layout_put(layout, GTK_WIDGET (wszystko), 205, 0);
    }
    else
    {
        GdkColor color;
        gdk_color_parse ("#381611", &color);
        gtk_widget_modify_bg (GTK_WIDGET (EkrStart), GTK_STATE_NORMAL, &color);
        gtk_container_add (GTK_CONTAINER (EkrStart), GTK_WIDGET (wszystko));
    }
///---------------------------------------------------------------------------------------------------
    gtk_widget_show_all (GTK_WIDGET(EkrStart));
    gtk_widget_grab_focus (GTK_WIDGET(EkrStart));

    g_timeout_add(100, przesunpasek, pasek);
    g_timeout_add(1111, zmienTekst, kliknij);

    wczytajGlobale();
return;
}

