#ifndef OKIENKA_H
#define OKIENKA_H

#include <gtk/gtk.h>

#include <stdbool.h>

#include "../globalne.h"
#include "../Funkcje/funkcje_conf.h"
#include "../Funkcje/zapis_gry.h"
#include "../Struktury/plansza.h"
#include "../Struktury/ranking.h"
#include "../GUI/inicjacja.h"
#include "../GUI/gra.h"

extern Plansza plansza;

void CzyKoniec (GtkWidget *widget, gpointer dane);
void CzyKoniecGry (GtkWidget *widget, gpointer dane);

void pokaz_blad(char *text);
void pokaz_info(GtkWindow *okno,char *text);
bool pokaz_pytanie(GtkWindow *okno, char *pytanie);

void Autor (GtkWidget *widget, gpointer dane);
void Ustawienia (GtkWidget *widget, gpointer dane);
void Wyniki (GtkWidget *widget, gpointer dane);
void Zasady(GtkWidget *widget, gpointer dane);


void ZapiszPlik(GtkWidget *widget, gpointer dane);
void WczytajPlik(GtkWidget *widget, gpointer dane);

void ustawOkno(GtkWindow *okno, char *tytul, gboolean mozliwosc_zmany_rozmiaru);

#endif // OKIENKA_H
