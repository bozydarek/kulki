#ifndef GRA_H
#define GRA_H

#include <gtk/gtk.h>

#include "../globalne.h"
#include "../Funkcje/funkcje.h"
#include "../Funkcje/funkcje_conf.h"
#include "../Funkcje/funkcje_gry.h"
#include "../Struktury/plansza.h"
#include "../Struktury/gracz.h"

void graj(GtkWidget *widget, gpointer cos);
void konamiSup();

#endif // GRA_H
