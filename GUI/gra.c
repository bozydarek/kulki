#include "gra.h"
///---------- Dane Globalne -------------
GtkImage *noweKulki[3], *KulkiNaPlanszy[9][9], *konamiImage;
GtkLabel *LiczbyWynik;

Plansza plansza;
///--------------------------------------
static void key_event(GtkWidget *widget, GdkEventKey *event) ///obsluga klawiszy w oknie gry
{
    if(strcmp(gdk_keyval_name (event->keyval), "Escape")==0)
        CzyKoniecGry(widget,NULL);

    if(strcmp(gdk_keyval_name (event->keyval), "q")==0)
    {
        gtk_image_set_from_pixbuf(konamiImage, konami);
    }
    if(strcmp(gdk_keyval_name (event->keyval), "w")==0)
    {
        gtk_image_clear(konamiImage);
    }
    if(plansza->TrwaGra == false)
    {
        if(strcmp(gdk_keyval_name (event->keyval), "Up")==0)
        {
            if(konamP<2)    konamP++;
            else            konamP=0;
        }
        if(strcmp(gdk_keyval_name (event->keyval), "Down")==0)
        {
            if(konamP == 2 || konamP == 3)  konamP++;
            else                            konamP=0;
        }
        if(strcmp(gdk_keyval_name (event->keyval), "Left")==0)
        {
            if(konamP == 4 || konamP == 6)  konamP++;
            else                            konamP=0;
        }
        if(strcmp(gdk_keyval_name (event->keyval), "Right")==0)
        {
            if(konamP == 5 || konamP == 7)  konamP++;
            else                            konamP=0;
        }
        if(strcmp(gdk_keyval_name (event->keyval), "B")==0)
        {
            if(konamP == 8) konamP++;
            else            konamP=0;
        }
        if(strcmp(gdk_keyval_name (event->keyval), "A")==0)
        {
            if(konamP == 9) konamiSup();
            else            konamP=0;
        }
    }
}

void graj(GtkWidget *widget, gpointer cos)
{
    (void) widget;

    srand(time(NULL));
    konamP = 0;

    if(cos == NULL)
        plansza = inicjujPlansze(poziom, ILE_NA_START);

    wypiszPlansze(plansza);
///------------------------------ GTK ------------------------------------------------
    GtkWindow *oknoGry = (GtkWindow *)gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_modal (oknoGry, TRUE);

    ustawOkno(oknoGry, "Kulki", TRUE);

    if(pelenEkran)
        gtk_window_fullscreen(oknoGry);
    else
        gtk_window_set_default_size(oknoGry, 1440, 900);

    g_signal_connect (oknoGry, "delete_event", G_CALLBACK (CzyKoniecGry),NULL);
    g_signal_connect (oknoGry, "key-release-event", G_CALLBACK(key_event), NULL);
///-------------------------------------------------------------------------------------
    GtkWidget *vseparator = gtk_separator_new (GTK_ORIENTATION_VERTICAL);

    GtkBox *wszystko = (GtkBox*)gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_container_add(GTK_CONTAINER(oknoGry), (GtkWidget*)wszystko);

///-------------------------------------------------------------------------------------
    GtkBox *Logo = (GtkBox*)gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
    gtk_box_pack_start(GTK_BOX(Logo), (GtkWidget*)gtk_image_new_from_pixbuf(logoBCGS), TRUE, TRUE, 25);
    gtk_box_pack_start(GTK_BOX(Logo), (GtkWidget*)gtk_label_new (copyright), TRUE, TRUE, 0);
///-------------------------------------------------------------------------------------
    GtkBox *Wynik = (GtkBox*)gtk_box_new(GTK_ORIENTATION_VERTICAL,10);

    GtkLabel *TekstWynik = (GtkLabel*)gtk_label_new(napisy[KULKI]);
    ustawNapis(TekstWynik, napisy[TWOJ_WYNIK], "25", KOLOR_TEM, "Times New Roman");

    gtk_box_pack_start(GTK_BOX(Wynik), (GtkWidget*)TekstWynik, TRUE, TRUE, 0);
    //-----------------------------------------------------------------------
    LiczbyWynik = (GtkLabel*)gtk_label_new(napisy[KULKI]);
    ustawNapis(LiczbyWynik, int2str(plansza->wynik), "25", KOLOR_TEM, "Times New Roman");

    gtk_box_pack_start(GTK_BOX(Wynik), (GtkWidget*)LiczbyWynik, TRUE, TRUE, 0);
///-------------------------------------------------------------------------------------
    GtkBox *Rekord = (GtkBox*)gtk_box_new(GTK_ORIENTATION_VERTICAL,10);

    GtkLabel *TekstRekord = (GtkLabel*)gtk_label_new(napisy[KULKI]);
    ustawNapis(TekstRekord, napisy[REKORD], "25", KOLOR_TEM, "Times New Roman");

    gtk_box_pack_start(GTK_BOX(Rekord), (GtkWidget*)TekstRekord, TRUE, TRUE, 0);
    //-----------------------------------------------------------------------
    GtkLabel *LiczbyRekord = (GtkLabel*)gtk_label_new(napisy[KULKI]);
    ustawNapis(LiczbyRekord, int2str(tablicaWynikow[poziom-1][0] != NULL ? tablicaWynikow[poziom-1][0]->wynik : 0), "25", KOLOR_TEM, "Times New Roman");

    gtk_box_pack_start(GTK_BOX(Rekord), (GtkWidget*)LiczbyRekord, TRUE, TRUE, 0);
///-------------------------------------------------------------------------------------
    GtkBox *Nowe_Kulki = (GtkBox*)gtk_box_new(GTK_ORIENTATION_VERTICAL,10);

    GtkLabel *Nast = (GtkLabel*)gtk_label_new(napisy[KULKI]);
    ustawNapis(Nast, napisy[NASTKULK], "25", KOLOR_TEM, "Times New Roman");

    gtk_box_pack_start(GTK_BOX(Nowe_Kulki), (GtkWidget*)Nast, TRUE, TRUE, 0);
///-------------------------------------------------------------------------------------
    GtkGrid *tabela = (GtkGrid *)gtk_grid_new();
    gtk_grid_set_column_homogeneous (tabela, TRUE);
    for(int i = 0; i<3; i++)
        gtk_grid_attach (tabela, GTK_WIDGET(noweKulki[i]=(GtkImage*)gtk_image_new_from_pixbuf(kulki[BRAK])), i, 0, 1, 1);

    gtk_box_pack_start(GTK_BOX(Nowe_Kulki), (GtkWidget*)tabela, TRUE, TRUE, 30);
///-------------------------------------------------------------------------------------
    GtkGrid *grid = (GtkGrid*)gtk_grid_new();
    gtk_grid_set_row_homogeneous (grid, TRUE);
    gtk_grid_set_row_spacing ( grid, 10 );

    GtkMenu * pasek = (GtkMenu *) gtk_menu_bar_new ();

    gtk_grid_attach( grid, GTK_WIDGET(pasek), 0, 0, 1, 1);
    gtk_grid_attach( grid, vseparator, 1, 0, 1, 14);
    gtk_grid_attach( grid, GTK_WIDGET(Wynik), 0, 1, 1, 2);
    gtk_grid_attach( grid, gtk_separator_new (GTK_ORIENTATION_HORIZONTAL), 0, 3, 2, 1);
    gtk_grid_attach( grid, GTK_WIDGET(Rekord), 0, 4, 1, 2);
    gtk_grid_attach( grid, gtk_separator_new (GTK_ORIENTATION_HORIZONTAL), 0, 6, 2, 1);
    gtk_grid_attach( grid, GTK_WIDGET(Nowe_Kulki), 0, 7, 1, 3);
    gtk_grid_attach( grid, gtk_separator_new (GTK_ORIENTATION_HORIZONTAL), 0, 10, 2, 1);
    gtk_grid_attach( grid, GTK_WIDGET(Logo), 0, 10, 1, 4);
    gtk_box_pack_start(GTK_BOX(wszystko), (GtkWidget*)grid, TRUE, TRUE, 0);
    ///-------------------------------------------------------------
    GtkMenu * menu = (GtkMenu *) gtk_menu_new ();

    GtkMenu * gra = (GtkMenu *) gtk_menu_item_new_with_mnemonic(napisy[GRA]);
    GtkMenu * nowa = (GtkMenu *) gtk_menu_item_new_with_mnemonic(napisy[1]);
    GtkMenu * wczytaj = (GtkMenu *) gtk_menu_item_new_with_mnemonic(napisy[WCZYTAJ]);
    GtkMenu * zapisz = (GtkMenu *) gtk_menu_item_new_with_mnemonic(napisy[ZAPISZ]);
    GtkMenu * wyjdz = (GtkMenu *) gtk_menu_item_new_with_mnemonic(napisy[6]);

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(gra), GTK_WIDGET(menu));
    gtk_menu_shell_append(GTK_MENU_SHELL(menu),  GTK_WIDGET(nowa));
    gtk_menu_shell_append(GTK_MENU_SHELL(menu),  GTK_WIDGET(wczytaj));
    gtk_menu_shell_append(GTK_MENU_SHELL(menu),  GTK_WIDGET(zapisz));
    gtk_menu_shell_append(GTK_MENU_SHELL(menu),  GTK_WIDGET(wyjdz));
    gtk_menu_shell_append(GTK_MENU_SHELL(pasek), GTK_WIDGET(gra));

    g_signal_connect (nowa, "activate", G_CALLBACK (nowaGra), oknoGry);
    g_signal_connect (wczytaj, "activate", G_CALLBACK (WczytajPlik), oknoGry);
    g_signal_connect (zapisz, "activate", G_CALLBACK (ZapiszPlik), oknoGry);
    g_signal_connect (wyjdz, "activate", G_CALLBACK (CzyKoniecGry),oknoGry);
    ///------------------------------
    GtkMenu * opcje = (GtkMenu *) gtk_menu_item_new_with_mnemonic(napisy[3]);

    gtk_menu_shell_append(GTK_MENU_SHELL(pasek), GTK_WIDGET(opcje));
    g_signal_connect (opcje, "activate", G_CALLBACK (Ustawienia), oknoGry);
    ///------------------------------
    GtkMenu * menu3 = (GtkMenu *) gtk_menu_new ();
    GtkMenu * pomoc = (GtkMenu *) gtk_menu_item_new_with_mnemonic(napisy[POMOC]);
    GtkMenu * zasady = (GtkMenu *) gtk_menu_item_new_with_mnemonic(napisy[ZASADY]);
    GtkMenu * oAutor = (GtkMenu *) gtk_menu_item_new_with_mnemonic(napisy[ZASADY+1]);

    gtk_menu_item_set_submenu(GTK_MENU_ITEM(pomoc), GTK_WIDGET(menu3));
    gtk_menu_shell_append(GTK_MENU_SHELL(menu3), GTK_WIDGET(zasady));
    gtk_menu_shell_append(GTK_MENU_SHELL(menu3), GTK_WIDGET(oAutor));
    gtk_menu_shell_append(GTK_MENU_SHELL(pasek), GTK_WIDGET(pomoc));

    g_signal_connect (zasady, "activate", G_CALLBACK (Zasady), oknoGry);
    g_signal_connect (oAutor, "activate", G_CALLBACK (Autor), NULL);
///-------------------------------------------------------------------------------------
    GtkGrid *SiatkaPlanszy = (GtkGrid *)gtk_grid_new();
    gtk_grid_set_row_spacing(SiatkaPlanszy, 10);
    gtk_grid_set_column_spacing(SiatkaPlanszy, 10);

    for(int i = 0; i<19; i+=2)
        gtk_grid_attach (SiatkaPlanszy, gtk_separator_new (GTK_ORIENTATION_VERTICAL), i, 0, 1, 19);
    for(int i = 0; i<21; i+=2)
        gtk_grid_attach (SiatkaPlanszy, gtk_separator_new (GTK_ORIENTATION_HORIZONTAL), 0, i, 19, 1);

///------------------------------------- USTAWIANIE KULEK ---------------------------------------
    for(int i = 1; i<19; i+=2)
        for(int j = 1; j<19; j+=2)
            gtk_grid_attach (SiatkaPlanszy,  GTK_WIDGET(KulkiNaPlanszy[i/2][j/2]=(GtkImage*)gtk_image_new_from_pixbuf(kulki[plansza->pola[i/2][j/2]->kolor])), j, i, 1, 1);

    for(int i = 0; i<3; i++)
        gtk_image_set_from_pixbuf(noweKulki[i],kulki[plansza->nowe[i]]);
///------------------------------------ USTAWIANIE EVENTÓW --------------------------------
    GtkEventBox * zdarzeniePlanszy = (GtkEventBox*) gtk_event_box_new();

    gtk_container_add(GTK_CONTAINER(zdarzeniePlanszy), (GtkWidget*)SiatkaPlanszy);

    g_signal_connect(G_OBJECT(zdarzeniePlanszy), "button_press_event", G_CALLBACK(naciskPlanszy), oknoGry);
    ///-------------------------------------------------------------------------------------
    GtkBox *strefaPl = (GtkBox*)gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_pack_start(GTK_BOX(strefaPl), (GtkWidget*)zdarzeniePlanszy, TRUE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(wszystko), (GtkWidget*)strefaPl, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(wszystko), (GtkWidget*)(konamiImage = (GtkImage*)gtk_image_new()), FALSE, FALSE, 0);
///-------------------------------------------------------------------------------------
    gtk_widget_show_all((GtkWidget*)oknoGry);

    if(wskazowki)
        pokaz_info(oknoGry, napisy[19]);
}

void konamiSup()
{
    gtk_image_set_from_pixbuf(konamiImage, konami);
    g_print("KONAMI CODE!!!\n");
    if(plansza->wynik >= 0)
        konamiEvent = true;
}
