#include "okienka.h"

void CzyKoniec(GtkWidget *widget, gpointer dane)
{
    (void) widget;
    (void) dane;

    GtkDialog *dialog = (GtkDialog*)gtk_message_dialog_new(GTK_WINDOW(okno), GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, "\n%s", napisy[10]);

    ustawOkno(GTK_WINDOW(dialog), napisy[INFO], FALSE);
    gtk_window_set_keep_above(GTK_WINDOW(dialog), TRUE);

    gint odp = gtk_dialog_run(GTK_DIALOG(dialog));

    gtk_widget_destroy(GTK_WIDGET(dialog));

    if (odp == GTK_RESPONSE_YES)
    {
        zapiszRanking();
        gtk_widget_destroy(GTK_WIDGET(okno));
        gtk_main_quit();
        exit(0);
    }
}

void CzyKoniecGry(GtkWidget *widget, gpointer dane)
{
    GtkWindow *okno;
    if(GTK_IS_WINDOW(widget))
        okno = (GtkWindow*)widget;
    else
        okno = (GtkWindow*)dane;

    GtkDialog *dialog = (GtkDialog*)gtk_message_dialog_new(GTK_WINDOW(okno), GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, "\n%s", napisy[10]);

    ustawOkno(GTK_WINDOW(dialog), napisy[INFO], FALSE);
    gtk_window_set_keep_above(GTK_WINDOW(dialog), TRUE);

    gint odp = gtk_dialog_run(GTK_DIALOG(dialog));

    gtk_widget_destroy(GTK_WIDGET(dialog));

    if (odp == GTK_RESPONSE_YES)
    {
        gtk_widget_destroy(GTK_WIDGET(okno));
    }

}

void pokaz_blad(char *text)
{
    GtkDialog *dialog = (GtkDialog*)gtk_message_dialog_new(GTK_WINDOW(okno), GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, "\n%s %s", napisy[9], text);

    if(strlen(napisy[BLAD])<=1)
        strcpy(napisy[BLAD], "Błąd/Error");

    gtk_window_set_keep_above(GTK_WINDOW(dialog), TRUE);

    ustawOkno(GTK_WINDOW(dialog), napisy[BLAD], FALSE);

    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(GTK_WIDGET(dialog));
}

void pokaz_info(GtkWindow *okno,char *text)
{
    GtkDialog *dialog = (GtkDialog*)gtk_message_dialog_new(GTK_WINDOW(okno), GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_INFO, GTK_BUTTONS_OK, "\n%s\n", text);

    ustawOkno(GTK_WINDOW(dialog), napisy[INFO], FALSE);

    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(GTK_WIDGET(dialog));
}

bool pokaz_pytanie(GtkWindow *okno, char *pytanie)
{
    GtkDialog *dialog = (GtkDialog*)gtk_message_dialog_new(GTK_WINDOW(okno), GTK_DIALOG_DESTROY_WITH_PARENT, GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, "\n%s", pytanie);

    ustawOkno(GTK_WINDOW(dialog), napisy[INFO], FALSE);
    gtk_window_set_keep_above(GTK_WINDOW(dialog), TRUE);

    gint odp = gtk_dialog_run(GTK_DIALOG(dialog));

    gtk_widget_destroy(GTK_WIDGET(dialog));

    if (odp == GTK_RESPONSE_YES)
        return true;
    return false;
}

void Autor(GtkWidget *widget, gpointer dane)
{
    (void) widget;
    (void) dane;
    GtkAboutDialog *about_dialog = (GtkAboutDialog*)gtk_about_dialog_new();

    gtk_about_dialog_set_logo(about_dialog,logoBCGS);

    gtk_about_dialog_set_comments ( about_dialog, "Gra wykonana jako projekt na pracownie C na rok 2014-15\nIndex: 273023\n");
    gtk_about_dialog_set_program_name ( about_dialog, napisy[KULKI]);
    gtk_about_dialog_set_copyright ( about_dialog, copyright);
    gtk_about_dialog_set_authors ( about_dialog, autor);
    gtk_about_dialog_set_version ( about_dialog, wersja);
    gtk_about_dialog_add_credit_section ( about_dialog, "Specialne podziękowania dla:", specialni);
    gtk_about_dialog_add_credit_section ( about_dialog, "Alpha/Beta-Testerzy:", betaTesterzy);
    gtk_about_dialog_set_website_label  ( about_dialog, napisy[15]);
    gtk_about_dialog_set_website ( about_dialog, "http://pl.wikipedia.org/wiki/Kulki_(gra_komputerowa)");

    gtk_dialog_run(GTK_DIALOG(about_dialog));
    gtk_widget_destroy(GTK_WIDGET(about_dialog));
}

static void aktywujWsk (GObject *przelacznik, GParamSpec *pspec, gpointer data)
{
    (void) pspec;

    bool *zmienna = data;
    if (gtk_switch_get_active (GTK_SWITCH (przelacznik)))
        (*zmienna) = true;
    else
        (*zmienna) = false;

    zapiszConf();
}

static void zmianaJezyka (GtkComboBox *widget, gpointer user_data)
{
    (void)user_data;
    GtkComboBoxText *lista = (GtkComboBoxText *)widget;

    if (gtk_combo_box_get_active (GTK_COMBO_BOX(lista)) != 0)
    {
        gchar *wybrany = gtk_combo_box_text_get_active_text (lista);
        g_print ("DEBUG - Zmiana języka na %s\n", wybrany);
        strcpy(jezyk, wybrany);
        zapiszConf();
        {
            if(strcmp(wybrany, "polski")==0)
                pokaz_info(okno, "Język zmieni się przy ponownym włączeniu programu");
            else if(strcmp(wybrany, "english")==0)
                pokaz_info(okno, "Language will change after restarting the program");
        }
        g_free (wybrany);
    }
}
static void zmianaPoziomu (GtkComboBox *widget, gpointer dane)
{
    GtkComboBox *lista = widget;
    int *zmienna = dane;

    if (gtk_combo_box_get_active (lista) != 0)
    {
        gchar *wybrany = gtk_combo_box_text_get_active_text (GTK_COMBO_BOX_TEXT(lista));
        g_print ("DEBUG - Zmiana poziomu na %s\n", wybrany);
        (*zmienna) = gtk_combo_box_get_active(lista);
        zapiszConf();

        g_free (wybrany);
    }
}

void Ustawienia(GtkWidget *widget, gpointer dane)
{
    (void)widget;

    bool wCzasieGry = false;

    if(dane!= NULL)
        wCzasieGry = true;

    GtkComboBox *listaJezykow, *listaPoziomow, *listaSzybkosci;

    GtkWindow* oknoUst = (GtkWindow*)gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_accept_focus (oknoUst , TRUE);

    ustawOkno(oknoUst, napisy[3], FALSE);

    gtk_window_set_modal (oknoUst, TRUE);

    gtk_window_set_default_size (oknoUst, 200, 100);
    gtk_container_set_border_width (GTK_CONTAINER (oknoUst), 10);

    listaJezykow = (GtkComboBox*) gtk_combo_box_text_new ();
    const char *opcja[] = {napisy[WYBIERZ],"polski", "english"};
    const char *poziomy[] = {napisy[WYBIERZ],napisy[21],napisy[22],napisy[23],napisy[24],napisy[25],napisy[26],napisy[27],napisy[28]};
    const char *poziomySzybkosci[] = {napisy[WYBIERZ], napisy[SZYBKOSC+1], napisy[SZYBKOSC+2],napisy[SZYBKOSC+3]};

    for (unsigned int i = 0; i < G_N_ELEMENTS (opcja); i++)
    {
        gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (listaJezykow), opcja[i]);
    }

    int ktore = 0;

    for(int i=1; i<3; i++)
        if(strcmp(opcja[i], jezyk)==0)
            ktore = i;

    gtk_combo_box_set_active (GTK_COMBO_BOX (listaJezykow), ktore);

    g_signal_connect (GTK_COMBO_BOX(listaJezykow), "changed", G_CALLBACK (zmianaJezyka), NULL);
    ///-----------------------------------------------------------------
    listaPoziomow = (GtkComboBox*) gtk_combo_box_text_new ();
    for (unsigned int i = 0; i < G_N_ELEMENTS (poziomy); i++)
    {
        gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (listaPoziomow), poziomy[i]);
    }

    gtk_combo_box_set_active (GTK_COMBO_BOX (listaPoziomow), poziom);

    g_signal_connect (GTK_COMBO_BOX(listaPoziomow), "changed", G_CALLBACK (zmianaPoziomu), &poziom);
    ///-----------------------------------------------------------------
    listaSzybkosci = (GtkComboBox*) gtk_combo_box_text_new ();
    for (unsigned int i = 0; i < G_N_ELEMENTS (poziomySzybkosci); i++)
    {
        gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (listaSzybkosci), poziomySzybkosci[i]);
    }

    gtk_combo_box_set_active (GTK_COMBO_BOX (listaSzybkosci), szybkosc);

    g_signal_connect (GTK_COMBO_BOX(listaSzybkosci), "changed", G_CALLBACK (zmianaPoziomu), &szybkosc);
    ///-----------------------------------------------------------------
    GtkSwitch *przelacznik = (GtkSwitch *)gtk_switch_new ();
    gtk_switch_set_active (GTK_SWITCH (przelacznik), (gboolean)wskazowki);
    GtkSwitch *przelacznik2 = (GtkSwitch *)gtk_switch_new ();
    gtk_switch_set_active (GTK_SWITCH (przelacznik2), (gboolean)pelenEkran);

    ///-----------------------------------------------------------------
    GtkGrid *siatka = (GtkGrid*)gtk_grid_new();
    gtk_grid_set_column_spacing (siatka, 20);
    gtk_grid_attach (siatka, GTK_WIDGET(gtk_label_new (napisy[11])), 0, 0, 1, 1);
    gtk_grid_attach (siatka, GTK_WIDGET(listaJezykow), 1, 0, 1, 1);
    gtk_grid_attach (siatka, GTK_WIDGET(gtk_label_new (napisy[POZIOM])), 0, 1, 1, 1);
    gtk_grid_attach (siatka, GTK_WIDGET(listaPoziomow), 1, 1, 1, 1);
    gtk_grid_attach (siatka, GTK_WIDGET(gtk_label_new (napisy[SZYBKOSC])), 0, 2, 1, 1);
    gtk_grid_attach (siatka, GTK_WIDGET(listaSzybkosci), 1, 2, 1, 1);
    gtk_grid_attach (siatka, GTK_WIDGET(gtk_label_new (napisy[12])), 0, 3, 1, 1);
    gtk_grid_attach (siatka, GTK_WIDGET(przelacznik), 1, 3, 1, 1);
    gtk_grid_attach (siatka, GTK_WIDGET(gtk_label_new (napisy[13])), 0, 4, 1, 1);
    gtk_grid_attach (siatka, GTK_WIDGET(przelacznik2), 1, 4, 1, 1);

    ///-----------------------------------------------------------------
    g_signal_connect (GTK_SWITCH (przelacznik), "notify::active", G_CALLBACK (aktywujWsk),  &wskazowki);
    g_signal_connect (GTK_SWITCH (przelacznik2), "notify::active", G_CALLBACK (aktywujWsk),  &pelenEkran);

    gtk_container_add (GTK_CONTAINER (oknoUst), GTK_WIDGET (siatka));

    if(wCzasieGry)
    {
        gtk_widget_set_sensitive(GTK_WIDGET(listaJezykow), FALSE);
        gtk_widget_set_sensitive(GTK_WIDGET(listaPoziomow), FALSE);
        gtk_widget_set_sensitive(GTK_WIDGET(przelacznik2), FALSE);

        gtk_widget_set_events (GTK_WIDGET(oknoUst), GDK_FOCUS_CHANGE_MASK);
        g_signal_connect (G_OBJECT (oknoUst), "focus-out-event", G_CALLBACK (pozaPopUpem), NULL);
    }

    gtk_widget_show_all (GTK_WIDGET(oknoUst));
}

static void zmianaRankingu (GtkComboBox *widget, gpointer dane)
{
    GtkComboBox *lista = widget;
    ranking zmienna = (ranking)dane;

    if (gtk_combo_box_get_active (lista) != 0)
    {
        int poziomOgladany = gtk_combo_box_get_active (lista);
        gchar *wybrany = gtk_combo_box_text_get_active_text (GTK_COMBO_BOX_TEXT(lista));
        g_print ("DEBUG - Zmiana wyswietlania rankingu na poziom: %s\n", wybrany);

        for(int i=0; i<10; i++)
            {
                ustawNapis(zmienna[i]->numer, int2str(i+1), "18", "black", "Times New Roman");
                if(tablicaWynikow[poziomOgladany-1][i]!=NULL)
                {
                    ustawNapis(zmienna[i]->wynik, int2str(tablicaWynikow[poziomOgladany-1][i]->wynik), "18", "black", "Times New Roman");
                    ustawNapis(zmienna[i]->etykietaGracza, tablicaWynikow[poziomOgladany-1][i]->nazwa, "13", "black", "Times New Roman");
                }
                else
                {
                    ustawNapis(zmienna[i]->wynik, "", "18", "black", "Times New Roman");
                    ustawNapis(zmienna[i]->etykietaGracza, napisy[LOSOWYRANK+losuj(0,3)], "13", "black", "Times New Roman");
                }
        }
    g_free (wybrany);
    }
}

void Wyniki(GtkWidget *widget, gpointer dane)
{
    (void) widget;
    (void) dane;

    GtkWindow *oknoWyn = (GtkWindow*)gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(oknoWyn), 400, 600);
    ustawOkno(GTK_WINDOW(oknoWyn), napisy[2], TRUE);

    gtk_widget_set_events (GTK_WIDGET(oknoWyn), GDK_FOCUS_CHANGE_MASK);
    g_signal_connect (G_OBJECT (oknoWyn), "focus-out-event", G_CALLBACK (pozaPopUpem), NULL);
    ///gtk_window_set_modal (oknoUst, TRUE);

    GtkBox    *wszystko = (GtkBox *)gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
    GtkLabel  *naglowek = (GtkLabel *)gtk_label_new(napisy[2]),
              *wybierzPoziom = (GtkLabel *)gtk_label_new(""),
              *poz = (GtkLabel *)gtk_label_new(""),
              *wynik = (GtkLabel *)gtk_label_new("");

    ustawNapis(naglowek, napisy[2], "50", KOLOR_TEM, "Times New Roman");
    ustawNapis(wybierzPoziom, napisy[39], "20", KOLOR_TEM, "Times New Roman");
    ustawNapis(poz, napisy[41], "20", KOLOR_TEM, "Times New Roman");
    ustawNapis(wynik, napisy[40], "20", KOLOR_TEM, "Times New Roman");

    gtk_box_pack_start(wszystko, GTK_WIDGET(naglowek), TRUE, TRUE, 10);
    gtk_box_pack_start(wszystko, GTK_WIDGET(wybierzPoziom), TRUE, TRUE, 0);

    //GdkColor color;
    //gdk_color_parse ("#E3E0CF", &color);
    //gtk_widget_modify_bg (GTK_WIDGET (oknoWyn), GTK_STATE_NORMAL, &color);
    ///-----------------------------------------------------------------
    const char *poziomy[] = {napisy[WYBIERZ],napisy[21],napisy[22],napisy[23],napisy[24],napisy[25],napisy[26],napisy[27],napisy[28]};
    int poziomOgladany = poziom;

    GtkComboBox *listaPoziomow = (GtkComboBox*) gtk_combo_box_text_new ();
    for (unsigned int i = 0; i < G_N_ELEMENTS (poziomy); i++)
    {
        gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (listaPoziomow), poziomy[i]);
    }

    gtk_combo_box_set_active (GTK_COMBO_BOX (listaPoziomow), poziomOgladany);
    gtk_box_pack_start(wszystko, GTK_WIDGET(listaPoziomow), TRUE, FALSE, 0);

    ///-----------------------------------------------------------------
    GtkGrid *siatka = (GtkGrid *)gtk_grid_new();

    gtk_grid_set_column_spacing (siatka, 20);
    gtk_grid_set_column_homogeneous(siatka, TRUE);
    gtk_grid_set_row_homogeneous(siatka, TRUE);
    gtk_grid_attach (siatka, GTK_WIDGET(poz), 0, 0, 1, 1);
    gtk_grid_attach (siatka, GTK_WIDGET(wynik), 2, 0, 1, 1);

    ranking wyswietlany = inicjujRanking(10);

    for(int i=0; i<10; i++)
    {
        gtk_grid_attach (siatka, GTK_WIDGET(wyswietlany[i]->numer), 0, i+1, 1, 1);
        gtk_grid_attach (siatka, GTK_WIDGET(wyswietlany[i]->etykietaGracza), 1,  i+1, 1, 1);
        gtk_grid_attach (siatka, GTK_WIDGET(wyswietlany[i]->wynik), 2,  i+1, 1, 1);

        ustawNapis(wyswietlany[i]->numer, int2str(i+1), "18", "black", "Times New Roman");
        if(tablicaWynikow[poziomOgladany-1][i]!=NULL)
        {
            ustawNapis(wyswietlany[i]->wynik, int2str(tablicaWynikow[poziomOgladany-1][i]->wynik), "18", "black", "Times New Roman");
            ustawNapis(wyswietlany[i]->etykietaGracza, tablicaWynikow[poziomOgladany-1][i]->nazwa, "13", "black", "Times New Roman");
        }
        else
        {
            ustawNapis(wyswietlany[i]->wynik, "", "18", "black", "Times New Roman");
            ustawNapis(wyswietlany[i]->etykietaGracza, napisy[LOSOWYRANK+losuj(0,3)], "13", "black", "Times New Roman");
            //break;
        }
    }
    gtk_box_pack_start(wszystko, GTK_WIDGET(siatka), TRUE, TRUE, 20);
    ///-----------------------------------------------------------------
    g_signal_connect (GTK_COMBO_BOX(listaPoziomow), "changed", G_CALLBACK (zmianaRankingu), wyswietlany);

    gtk_container_add (GTK_CONTAINER (oknoWyn), GTK_WIDGET (wszystko));

    gtk_widget_show_all (GTK_WIDGET(oknoWyn));
}

void Zasady(GtkWidget *widget, gpointer dane)
{
    (void)widget;
    (void)dane;
    //GtkWindow *NaOkno = (GtkWindow*)dane;

    GtkWindow *oknoZas = (GtkWindow*)gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(oknoZas), 400, 600);
    ustawOkno(GTK_WINDOW(oknoZas), napisy[ZASADY], FALSE);

    gtk_window_set_modal (oknoZas, TRUE);

    gtk_widget_set_events (GTK_WIDGET(oknoZas), GDK_FOCUS_CHANGE_MASK);
    g_signal_connect (G_OBJECT (oknoZas), "focus-out-event", G_CALLBACK (pozaPopUpem), NULL);

    GtkBox *wszystko = (GtkBox *)gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);

    GtkLabel *Tytul = (GtkLabel *)gtk_label_new("");
    ustawNapis(Tytul, napisy[ZASADY], "40", KOLOR_TEM, "Times New Roman");
    gtk_box_pack_start(wszystko, GTK_WIDGET(Tytul), FALSE, FALSE, 10);

    GtkLabel *t1 = (GtkLabel *)gtk_label_new("Gra jednoosobowa, rozgrywana na planszy 9x9 kwadratowych pól, na których\nsja w losowych miejscach pojawiają się kulki występujące w różnych kolorach.");
    gtk_box_pack_start(wszystko, GTK_WIDGET(t1), TRUE, FALSE, 10);

    gtk_box_pack_start(wszystko, GTK_WIDGET(gtk_image_new_from_animation(tutorial)), FALSE, FALSE, 10);

    GtkLabel *t2 = (GtkLabel *)gtk_label_new("Zadaniem gracza jest usuwanie tych kulek z planszy poprzez ustawianie obok \nsiebie w ciągłej linii(poziomej, pionowej lub ukośnej) pięciu lub więcej kulek \njednakowego koloru.");
    gtk_box_pack_start(wszystko, GTK_WIDGET(t2), TRUE, FALSE, 10);

    GtkLabel *t3 = (GtkLabel *)gtk_label_new("W jednym ruchu dowolną kulkę można przestawić na dowolne puste pole planszy,\no ile istnieje możliwość dojścia do niego poruszając się jedynie po sąsiednich\npustych polach planszy (sąsiednie pola przylegają do siebie bokiem).");
    gtk_box_pack_start(wszystko, GTK_WIDGET(t3), TRUE, FALSE, 10);

    GtkLabel *t4 = (GtkLabel *)gtk_label_new("Jeśli przestawienie powoduje usunięcie kulek z planszy, to gracz wykonuje od razu\nkolejny ruch,a jeśli nie powoduje, na planszy pojawiają się trzy nowe kulki, zanim\nmożna wykonać kolejny ruch.");
    gtk_box_pack_start(wszystko, GTK_WIDGET(t4), TRUE, FALSE, 10);

    GtkLabel *t5 = (GtkLabel *)gtk_label_new("");
    ustawNapis(t5,napisy[59],"20", KOLOR_TEM, "Times New Roman");
    gtk_box_pack_start(wszystko, GTK_WIDGET(t5), TRUE, FALSE, 10);

    gtk_container_add (GTK_CONTAINER (oknoZas), GTK_WIDGET (wszystko));

    gtk_widget_show_all (GTK_WIDGET(oknoZas));
}

void ZapiszPlik(GtkWidget *widget, gpointer dane)
{
    (void)widget;
    GtkWindow *NaOkno = (GtkWindow*)dane;

    GtkFileChooserDialog *WyborZapisu = (GtkFileChooserDialog *) gtk_file_chooser_dialog_new (napisy[ZAPISZ], NaOkno,
                                                                                             GTK_FILE_CHOOSER_ACTION_SAVE,
                                                                                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                                                                             GTK_STOCK_SAVE, GTK_RESPONSE_OK,
                                                                                             NULL);
    GtkFileFilter *filtrKulek = gtk_file_filter_new ();
    gtk_file_filter_add_pattern (filtrKulek, "*.kulki");
    gtk_file_filter_set_name(filtrKulek, napisy[PLIK]);

    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(WyborZapisu), filtrKulek);

    if (gtk_dialog_run (GTK_DIALOG (WyborZapisu)) == GTK_RESPONSE_OK)
    {
        char nazwa_pliku[MAX_PATH];
        strcpy(nazwa_pliku, (char*)gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (WyborZapisu)));

        dodajRozszerzenie(nazwa_pliku);

        printf("%s\n", nazwa_pliku);
    ///Zapisz
        int stan = zapiszGre(nazwa_pliku);
        if(stan != 0)
        {
            pokaz_blad(int2str(stan));
        }
    }
printf("Czyszczenie.");
    gtk_widget_destroy (GTK_WIDGET(WyborZapisu));
printf("OK");

}

void WczytajPlik(GtkWidget *widget, gpointer dane)
{
    (void)widget;

    GtkWindow *NaOkno;

    if(GTK_IS_WINDOW(dane))
        NaOkno = (GtkWindow*)dane;
    else
        NaOkno = okno;

    GtkFileChooserDialog *WyborWczyt = (GtkFileChooserDialog *) gtk_file_chooser_dialog_new (napisy[WCZYTAJ], GTK_WINDOW(NaOkno), GTK_FILE_CHOOSER_ACTION_OPEN, GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, NULL);

    GtkFileFilter *filtrKulek = gtk_file_filter_new ();
    gtk_file_filter_add_pattern (filtrKulek, "*.kulki");
    gtk_file_filter_set_name(filtrKulek, napisy[PLIK]);

    gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(WyborWczyt), filtrKulek);

    if (gtk_dialog_run (GTK_DIALOG (WyborWczyt)) == GTK_RESPONSE_ACCEPT)
    {
        char *nazwa_pliku;
        nazwa_pliku = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (WyborWczyt));
        ///Wczytaj

        int stan = wczytajGre(nazwa_pliku);
        if(stan == 3)
        {
            pokaz_blad("Nieładnie! Ktoś tu próbował, oszukiwać!");
            stan = 0;
        }
        if(stan != 0)
        {
            pokaz_blad(int2str(stan));
        }
        else
        {
            if(plansza == NULL)
                exit(666);

            if(GTK_IS_WINDOW(dane))
                gtk_widget_destroy (GTK_WIDGET(NaOkno));

            graj(NULL,plansza);
        }
        g_free (nazwa_pliku);
    }

    gtk_widget_destroy (GTK_WIDGET(WyborWczyt));
}

void ustawOkno(GtkWindow *okno, char *tytul, gboolean mozliwosc_zmany_rozmiaru)
{
    gtk_window_set_title(okno, tytul);
    gtk_window_set_icon(okno, ikonka);
    gtk_window_set_position(okno, GTK_WIN_POS_CENTER);
    gtk_window_set_resizable(okno, mozliwosc_zmany_rozmiaru);
}
