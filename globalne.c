#include "globalne.h"

const int punktacja[10] = { 0, 10, 20, 30, 40, 50, 60, 75, 90, 120};
const int liczbyPierwsze[10] = {11, 13, 17, 19, 23, 29, 31, 37, 41, 43};

char jezyk[10] = {"polski"};
char napisy[ILOSC_NAPISOW][100];

const gchar wersja[20] = "Beta 1.0.2";
const gchar copyright[50] = "Copyright © 2015 BożydarCompany Game Studio";
const gchar *autor[40] = {"Piotr \"Bożydar\" Szymajda - 273023", NULL};
const gchar *specialni[20]= {"Cielak","Dybiec","Kajo",NULL};
const gchar *betaTesterzy[20] = {"Asdamos","Artix","Kajo","Nati","Zalesia",NULL};

