#ifndef ZAPIS_GRY_H
#define ZAPIS_GRY_H

#include <stdlib.h>

#include "../globalne.h"
#include "../Struktury/plansza.h"

extern Plansza plansza;

int zapiszGre (char *sciezka);
int wczytajGre(char *sciezka);

#endif // ZAPIS_GRY_H
