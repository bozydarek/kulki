#ifndef FUNKCJE_CONF_H
#define FUNKCJE_CONF_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "../globalne.h"
#include "../Struktury/gracz.h"
#include "../GUI/okienka.h"

extern gracz * tablicaWynikow[8][10];

bool wczytajConf();
void domyslnaConf();
void wczytajNapisy();
void zapiszConf();
void wczytajZmiennaBool(FILE *plik, bool *zmienna);

void zapiszRanking();
bool wczytajRanking();
void ustawRakningDomysny();

#endif // FUNKCJE_CONF_H
