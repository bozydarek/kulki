#ifndef FUNKCJE_GRY_H
#define FUNKCJE_GRY_H

#include <gtk/gtk.h>
#include <stdbool.h>

#include "../globalne.h"

#include "../Funkcje/funkcje.h"
#include "../Struktury/plansza.h"
#include "../Struktury/kolejka.h"
#include "../GUI/okienka.h"

extern GtkImage *noweKulki[3], *KulkiNaPlanszy[9][9], *konamiImage;
extern GtkLabel *LiczbyWynik;

extern Plansza plansza;

gboolean naciskPlanszy(GtkWidget *event_box, GdkEventButton *event, gpointer data);
bool szukanieDrogi(Plansza plansza, int x, int y);
void wyznaczDroge( int x, int y);
gboolean przemiescKulke(gpointer oknoGry);
void usunFlagi(Plansza plansza);
void rysujKulki(Plansza plansza);
void dajNoweKulki(Plansza plansza, gpointer oknoGry);
bool usunKulki(Plansza plansza);

void nowaGra(GtkWidget *widget, gpointer dane);
gboolean wybuch(GtkWidget *widget, gpointer dane);
void wybuchy(int x, int y, int sila);

void KoniecGry(GtkWindow * oknoGry);
void PrzegralesZKretesem(GtkWindow *);
void DoRankingu(GtkWindow * oknoGry);

#endif // FUNKCJE_GRY_H
