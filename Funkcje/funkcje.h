#ifndef FUNKCJE_H
#define FUNKCJE_H

#include <gtk/gtk.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "../globalne.h"

void wypiszBlad(void *widzet, GError* blad);
void ustawNapis(GtkLabel * Etykieta, char tekst[], char rozmiar[], char kolor[], char czcionka[]);
int losuj(int min, int max);

char* int2str(int n);
void dodajRozszerzenie(char * nazwa);

gboolean naciskN(GtkWidget *event_box, GdkEventButton *wydarzenie, gpointer data);
gboolean pozaPopUpem (GtkWidget *widget, GdkEventFocus *wydarzenie, gpointer dane);
gboolean rozpocznj (GtkWidget *widget, GdkEventFocus *wydarzenie, gpointer dane);



#endif // FUNKCJE_H
