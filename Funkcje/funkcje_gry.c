#include "funkcje_gry.h"

gboolean naciskPlanszy(GtkWidget *event_box, GdkEventButton *event, gpointer data)
{
    (void) event_box;

    GtkWindow  * oknoGry = (GtkWindow *) data;

    int x = event->y/90, y = event->x/90;

    //wypiszPlansze(plansza);

    if(x>8 || y>8)
        return FALSE;

    if(konamiEvent)
    {
        konamiEvent = false;
        gtk_image_set_from_animation(KulkiNaPlanszy[x][y], boom);
        plansza->aktywnePole = plansza->pola[x][y];

        plansza ->wynik -= 666;
        gtk_image_clear(konamiImage);

        if(plansza->wynik < 0)
        {
            ustawNapis(LiczbyWynik, "0", "25", KOLOR_TEM, "Times New Roman");
            PrzegralesZKretesem(oknoGry);
        }
        else
        {
            ustawNapis(LiczbyWynik, int2str(plansza->wynik), "25", KOLOR_TEM, "Times New Roman");
            g_timeout_add(2200, (GSourceFunc) wybuch, (gpointer) NULL);
        }
        return TRUE;
    }

    if(plansza->TrwaPrzemieszczenie == true || plansza->TrwaGra == false)
        return FALSE;

    if(plansza->aktywnePole == NULL)
    {
        if(plansza->pola[x][y]->kolor != 0)
            plansza->aktywnePole = plansza->pola[x][y];
    }
    else
    {
        if(plansza->pola[x][y]->kolor != 0)
        {
            gtk_image_set_from_pixbuf(KulkiNaPlanszy[plansza->aktywnePole->x][plansza->aktywnePole->y],kulki[plansza->aktywnePole->kolor]);
            plansza->aktywnePole = plansza->pola[x][y];
        }
        else
        {/// Masz aktywną kulkę i kliknąłeś puste pole - trzeba znaleźć drogę.
            g_print("Szukam drogi.\n");
            if(szukanieDrogi(plansza, x,y))
            {
                g_print("Znalazlem!\n");
                wyznaczDroge(x,y);
                usunFlagi(plansza);

                plansza->TrwaPrzemieszczenie = true;

                g_timeout_add(50+(szybkosc-1)*100, (GSourceFunc) przemiescKulke, (gpointer) oknoGry);
            }
            else
                usunFlagi(plansza);
        }
    }
    if(plansza->aktywnePole != NULL)
    {
        GdkPixbuf * zaznaczenie = gdk_pixbuf_copy(kulki[BRAK]);


        gdk_pixbuf_composite(kulki[plansza->aktywnePole->kolor], zaznaczenie, 0, 0 ,
                                    70, 70, 14, 14, 0.6, 0.6,
                                    GDK_INTERP_BILINEAR, 230);

        gtk_image_set_from_pixbuf(KulkiNaPlanszy[plansza->aktywnePole->x][plansza->aktywnePole->y],zaznaczenie);
    }
    return TRUE;
}

bool szukanieDrogi(Plansza plansza, int x, int y)
{
    kolejka KolejkaDrogi = inicjuj();
    int w, k;

    dodaj(KolejkaDrogi, plansza->aktywnePole->x);
    dodaj(KolejkaDrogi, plansza->aktywnePole->y);
    g_print("M-%d %d\n", x, y);
    g_print("S-%d %d\n", plansza->aktywnePole->x, plansza->aktywnePole->y);

    while(czyPusta(KolejkaDrogi) == false)
    {
        w = pobierz(KolejkaDrogi);
        k = pobierz(KolejkaDrogi);

        if(w==-1 || k==-1)
            exit(666);

        if(w==x && k==y)
        {
            niszcz(KolejkaDrogi);
            return true;
        }
        for(int i=-1; i<=1; i++)
            for(int j=-1; j<=1; j++)
                if((i==0 || j==0) && (i!=j))
                {
                    if(w+i>=0 && w+i<=8 && k+j>=0 && k+j<=8)
                        if(plansza->pola[w+i][k+j]->kolor == 0)
                        {
                            /// ROZSTAW FLAGI:
                            if      (i == -1) plansza->pola[w+i][k+j]->kolor = DOL;  // z dołu
                            else if (i ==  1) plansza->pola[w+i][k+j]->kolor = GORA;  // z góry
                            else if (j == -1) plansza->pola[w+i][k+j]->kolor = PRAWO;  // z prawej
                            else              plansza->pola[w+i][k+j]->kolor = LEWO;  // z lewej

                            //g_print("%d %d\n", w+i, k+j);
                            dodaj(KolejkaDrogi, w+i);
                            dodaj(KolejkaDrogi, k+j);
                        }
                }
    }
    niszcz(KolejkaDrogi);
    return false;
}
void wyznaczDroge(int x, int y)
{
    niszcz(plansza->trasa);

    plansza->trasa = inicjuj();

    int i = x, j = y;

    while((i != plansza->aktywnePole->x) || (j != plansza->aktywnePole->y))
    {
        int c =  plansza->pola[i][j]->kolor;
        dodajNaPoczatek(plansza->trasa,j);
        dodajNaPoczatek(plansza->trasa,i);
        switch(c)
        {
        case DOL :
            i++;
            break;
        case GORA :
            i--;
            break;
        case PRAWO :
            j++;
            break;
        case LEWO :
            j--;
            break;
        }
    }
}
gboolean przemiescKulke(gpointer oknoGry)
{
    if(czyPusta(plansza->trasa))
    {
        plansza->TrwaPrzemieszczenie = false;
        plansza->aktywnePole = NULL;
        dajNoweKulki(plansza, oknoGry);
        rysujKulki(plansza);
        return FALSE;
    }
    int x = pobierz(plansza->trasa);
    int y = pobierz(plansza->trasa);

    plansza->pola[x][y]->kolor = plansza->aktywnePole->kolor;
    plansza->aktywnePole->kolor = 0;

    plansza->aktywnePole = plansza->pola[x][y];
    rysujKulki(plansza);

    return TRUE;
}

void usunFlagi(Plansza plansza)
{
    for(int i = 0; i<9; i++)
        for(int j = 0; j<9; j++)
            if(plansza->pola[i][j]->kolor >= GORA)
                plansza->pola[i][j]->kolor = 0;
}
void dajNoweKulki(Plansza plansza, gpointer oknoGry)
{
    if(usunKulki(plansza)==false)
    {
        int ile = ileWolnychMiejsc(plansza);

        int ileNowych = (ile < 3 ? ile : 3);

        for(int i = 0; i<ileNowych; i++)
        {
            while(ustawKulkeLosowo(plansza, plansza->nowe[i])==false);

            plansza->nowe[i]=losuj(1,plansza->kolorow);
            gtk_image_set_from_pixbuf(noweKulki[i],kulki[plansza->nowe[i]]);
        }

        usunKulki(plansza); /// Dostawienie losowych może zniknąć kulki ;)

        ile = ileWolnychMiejsc(plansza);
        if(ile == 0)
        {
            g_print("KONIEC GRY\nWYNIK:\t%d\n", plansza->wynik);
            KoniecGry(GTK_WINDOW(oknoGry));
        }
    }
    ustawNapis(LiczbyWynik, int2str(plansza->wynik), "25", KOLOR_TEM, "Times New Roman");
}

void rysujKulki(Plansza plansza)
{
    for(int i = 0; i<9; i++)
        for(int j = 0; j<9; j++)
            gtk_image_set_from_pixbuf(KulkiNaPlanszy[i][j],kulki[plansza->pola[i][j]->kolor]);
}
static void czyscTabBool(int x, int y, bool usuniete[x][y])
{
    for(int i = 0; i<x; i++)
        for(int j = 0; j<y; j++)
            usuniete[i][j] = 0;
}

bool usunKulki(Plansza plansza)
{
    kolejka doUsuniecia = inicjuj();
    bool usuniete[9][9];
    czyscTabBool(9, 9, usuniete);

    int premia=0, punkty=0;
    ///SPR w DÓL:
    for(int i = 0; i<5; i++)
        for(int j = 0; j<9; j++)
        {
            int kolorPola = plansza->pola[i][j]->kolor;
            if(kolorPola!=0 && usuniete[i][j]==0)
            {
                int licz = 1;
                while(plansza->pola[i+licz][j]->kolor == kolorPola)
                {
                    licz++;
                    if (i+licz>8)
                        break;
                }
                if(licz>=ILE_ZNIKA)
                {
                    for(int k=0; k<licz; k++)
                    {
                        dodaj(doUsuniecia,i+k);
                        dodaj(doUsuniecia,j);
                        usuniete[i+k][j] = 1;
                    }
                    premia++;
                    punkty += punktacja[licz];
                }
            }
        }
    czyscTabBool(9, 9, usuniete);
    ///SPR w PRAWO:
    for(int i = 0; i<9; i++)
        for(int j = 0; j<5; j++)
        {
            int kolorPola = plansza->pola[i][j]->kolor;
            if(kolorPola!=0 && usuniete[i][j]==0)
            {
                int licz = 1;
                while(plansza->pola[i][j+licz]->kolor == kolorPola)
                {
                    licz++;
                    if (j+licz>8)
                        break;
                }
                if(licz>=ILE_ZNIKA)
                {
                    for(int k=0; k<licz; k++)
                    {
                        dodaj(doUsuniecia,i);
                        dodaj(doUsuniecia,j+k);
                        usuniete[i][j+k] = 1;
                    }
                    premia++;
                    punkty += punktacja[licz];
                }
            }
        }
    czyscTabBool(9, 9, usuniete);
    ///SPR w PRAWY-DOL:
    for(int i = 0; i<5; i++)
        for(int j = 0; j<5; j++)
        {
            int kolorPola = plansza->pola[i][j]->kolor;
            if(kolorPola!=0 && usuniete[i][j]==0)
            {
                int licz = 1;
                while(plansza->pola[i+licz][j+licz]->kolor == kolorPola)
                {
                    licz++;
                    if (i+licz>8 || j+licz>8)
                        break;
                }
                if(licz>=ILE_ZNIKA)
                {
                    for(int k=0; k<licz; k++)
                    {
                        dodaj(doUsuniecia,i+k);
                        dodaj(doUsuniecia,j+k);
                        usuniete[i+k][j+k] = 1;
                    }
                    premia++;
                    punkty += punktacja[licz];
                }
            }
        }
    czyscTabBool(9, 9, usuniete);
    ///SPR w LEWY-DOL:
    for(int i = 0; i<5; i++)
        for(int j = 4; j<9; j++)
        {
            int kolorPola = plansza->pola[i][j]->kolor;
            if(kolorPola!=0 && usuniete[i][j]==0)
            {
                int licz = 1;
                while(plansza->pola[i+licz][j-licz]->kolor == kolorPola)
                {
                    licz++;
                    if (i+licz>8 || j-licz<0)
                        break;
                }
                if(licz>=ILE_ZNIKA)
                {
                    for(int k=0; k<licz; k++)
                    {
                        dodaj(doUsuniecia,i+k);
                        dodaj(doUsuniecia,j-k);
                        usuniete[i+k][j-k] = 1;
                    }
                    premia++;
                    punkty += punktacja[licz];
                }
            }
        }
    ///----------------------------------
    if(czyPusta(doUsuniecia) == true)
        return false;

    while(czyPusta(doUsuniecia)==false)
    {
        int x = pobierz(doUsuniecia);
        int y = pobierz(doUsuniecia);

        plansza->pola[x][y]->kolor = 0;
    }
    niszcz(doUsuniecia);

    plansza->wynik += (punkty *(1.0+(premia-1)/5.0)*(1.0 + (poziom-3)/10.0));
    g_print("Wynik - %d\n", plansza->wynik);
    g_print("Dodano - %f\n", (int)punkty *(1.0+(premia-1)/5.0)*(1.0 + (poziom-3)/10.0));

return true;
}

void nowaGra(GtkWidget *widget, gpointer dane)
{
    (void) widget;
    GtkWindow * okno = (GtkWindow *) dane;

    if(pokaz_pytanie(okno, napisy[CZYNOWA]))
    {
        int kolor = plansza->kolorow;
        plansza = inicjujPlansze(kolor, ILE_NA_START);

        rysujKulki(plansza);
        ustawNapis(LiczbyWynik, int2str(plansza->wynik), "25", KOLOR_TEM, "Times New Roman");

        for(int i = 0; i<3; i++)
        {
            gtk_image_set_from_pixbuf(noweKulki[i],kulki[plansza->nowe[i]]);
        }
    }
}
gboolean wybuch(GtkWidget *widget, gpointer dane)
{
    (void) widget;
    (void) dane;

    plansza->aktywnePole->kolor=0;
    gtk_image_set_from_pixbuf(KulkiNaPlanszy[plansza->aktywnePole->x][plansza->aktywnePole->y],kulki[BRAK]);
    wybuchy(plansza->aktywnePole->x, plansza->aktywnePole->y, LEJ_PO_BOMBIE);
    wypiszPlansze(plansza);
    plansza->TrwaGra = true;
return FALSE;
}

void wybuchy(int x, int y, int sila)
{
    if(sila == 0)
        return;
    //if(sila != LEJ_PO_BOMBIE) // żeby wybuchało zawsze więcej niż jedna
        if(losuj(0,sila+2) < 1)
            return;

    plansza->pola[x][y]->kolor=0;
    gtk_image_set_from_pixbuf(KulkiNaPlanszy[x][y],kulki[BRAK]);

    for(int i=-1; i<=1; i++)
        for(int j=-1; j<=1; j++)
            if((i!=0 || j!=0))
                if(x+i>=0 && x+i<=8 && y+j>=0 && y+j<=8)
                    if(plansza->pola[x+i][y+j]->kolor != 0)
                        wybuchy(x+i,y+j, sila-1);
}

static void uruchomRanking (GtkWidget *widget, gpointer dane)
{
    GtkWindow *oknoG = (GtkWindow *)dane;
    gtk_widget_destroy (widget);
    if(czyDodwacDoWynikow(poziom, plansza->wynik) && konamiEvent == false && plansza->TrwaGra == false)
        DoRankingu(oknoG);
}

void KoniecGry(GtkWindow * oknoGry)
{
    plansza->TrwaGra = false;

    GtkWindow * oknoKonca = (GtkWindow *) gtk_window_new(GTK_WINDOW_POPUP);
    ustawOkno(oknoKonca,"",false);

    gtk_window_set_modal(oknoGry, FALSE);

    gtk_widget_set_size_request (GTK_WIDGET(oknoKonca), 1000, 150);

    gtk_widget_set_events (GTK_WIDGET(oknoKonca), GDK_BUTTON_PRESS_MASK );
    g_signal_connect (G_OBJECT (oknoKonca), "button-press-event", G_CALLBACK (uruchomRanking), (gpointer)oknoGry);

    GdkColor color;
    gdk_color_parse ("#3b3131", &color);
    gtk_widget_modify_bg (GTK_WIDGET (oknoKonca), GTK_STATE_NORMAL, &color);

    GtkLabel * przegrales = (GtkLabel *)gtk_label_new(""), * twojwynik = (GtkLabel *)gtk_label_new(""), * wynik = (GtkLabel *)gtk_label_new(""), *kliknij = (GtkLabel *)gtk_label_new("");
    ustawNapis(przegrales,napisy[PRZEGRALES],"85","#CCCCCC","Arial Black");
    ustawNapis(twojwynik, napisy[TWOJ_WYNIK],"40","#CCCCCC", "Arial");
    ustawNapis(wynik, int2str(plansza->wynik),"40","#CCCCCC", "Arial");
    ustawNapis(kliknij, napisy[KLIKNIJ],"20","#CCCCCC", "Arial");

    GtkBox * wszystko = (GtkBox *)gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    gtk_box_pack_start(wszystko, GTK_WIDGET (przegrales), TRUE, TRUE, 10);
    gtk_box_pack_start(wszystko, GTK_WIDGET (twojwynik), TRUE, TRUE, 20);
    gtk_box_pack_start(wszystko, GTK_WIDGET (wynik), TRUE, TRUE, 20);
    gtk_box_pack_start(wszystko, GTK_WIDGET (kliknij), TRUE, TRUE, 10);

    gtk_container_add (GTK_CONTAINER (oknoKonca), GTK_WIDGET (wszystko));
    gtk_widget_show_all (GTK_WIDGET(oknoKonca));
    gtk_widget_grab_focus (GTK_WIDGET (oknoKonca));
}

void PrzegralesZKretesem(GtkWindow * oknoGry)
{
    plansza->TrwaGra = false;

    GtkWindow * oknoKonca = (GtkWindow *)gtk_window_new(GTK_WINDOW_POPUP);
    ustawOkno(oknoKonca,"",false);

    gtk_widget_set_size_request (GTK_WIDGET(oknoKonca), 1000, 150);

    gtk_widget_set_events (GTK_WIDGET(oknoKonca), GDK_BUTTON_PRESS_MASK );
    g_signal_connect (G_OBJECT (oknoKonca), "button-press-event", G_CALLBACK (pozaPopUpem), oknoGry);

    GdkColor color;
    gdk_color_parse ("#3b3131", &color);
    gtk_widget_modify_bg (GTK_WIDGET (oknoKonca), GTK_STATE_NORMAL, &color);

    GtkLabel * przegrales = (GtkLabel *)gtk_label_new(""), * twojwynik = (GtkLabel *)gtk_label_new(""), * wynik = (GtkLabel *)gtk_label_new(""), *kliknij = (GtkLabel *)gtk_label_new("");
    ustawNapis(przegrales, napisy[PRZEGRALES],"55","#CCCCCC","Abstract pl");
    ustawNapis(twojwynik, napisy[56],"55","#CCCCCC", "Abstract pl");
    ustawNapis(wynik, napisy[57],"40","#CCCCCC", "Abstract pl");
    ustawNapis(kliknij, napisy[58],"20","#CCCCCC", "Abstract pl");

    GtkBox * wszystko = (GtkBox *)gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);
    gtk_box_pack_start(wszystko, GTK_WIDGET (przegrales), TRUE, TRUE, 20);
    gtk_box_pack_start(wszystko, GTK_WIDGET (twojwynik), TRUE, TRUE, 20);
    gtk_box_pack_start(wszystko, GTK_WIDGET (wynik), TRUE, TRUE, 20);
    gtk_box_pack_start(wszystko, GTK_WIDGET (kliknij), TRUE, TRUE, 10);

    gtk_container_add (GTK_CONTAINER (oknoKonca), GTK_WIDGET (wszystko));
    gtk_widget_show_all (GTK_WIDGET(oknoKonca));
    gtk_widget_grab_focus (GTK_WIDGET(oknoKonca));
}

static void zmknijDodajDoRank (GtkWidget *widget, gpointer dane)
{
    oknoRankingowe *nowe = (oknoRankingowe *) dane;

    if(!dodajGracza((char *)gtk_entry_get_text(nowe->wpisz_nazwe), (int)plansza->wynik, poziom, false))
        puts("O kurcze! Cos nie wyszlo! Sprawdz czy gracz mial dobra liczbe punktow.");

    gtk_widget_destroy (widget);
    gtk_widget_destroy (GTK_WIDGET(nowe->oknoDoRank));

    free(nowe);
}
void DoRankingu(GtkWindow * oknoGry)
{
    plansza->TrwaGra = false;
    konamiEvent = false;
    oknoRankingowe * nowe = malloc(sizeof(oknoRankingowe));
    nowe->oknoPodSpodem = oknoGry;

    nowe->oknoDoRank = (GtkWindow *) gtk_window_new(GTK_WINDOW_POPUP);
    ustawOkno(nowe->oknoDoRank,"",false);

    gtk_window_set_modal(nowe->oknoDoRank, TRUE);

    gtk_widget_set_size_request (GTK_WIDGET(nowe->oknoDoRank), 1000, 150);

    GdkColor color;
    gdk_color_parse ("#3b3131", &color);
    gtk_widget_modify_bg (GTK_WIDGET (nowe->oknoDoRank), GTK_STATE_NORMAL, &color);

    GtkLabel * brawo = (GtkLabel *)gtk_label_new(""), * dostal = (GtkLabel *)gtk_label_new(""), * podaj = (GtkLabel *)gtk_label_new(""), *kliknij = (GtkLabel *)gtk_label_new(""), *dodajDoRan = (GtkLabel *)gtk_label_new("");
    ustawNapis(brawo,napisy[51],"85","#CCCCCC","Arial Black");
    ustawNapis(dostal, napisy[52],"30","#CCCCCC", "Arial");
    ustawNapis(podaj, napisy[53],"30","#CCCCCC", "Arial");
    ustawNapis(kliknij, napisy[KLIKNIJ],"20","#CCCCCC", "Arial");
    ustawNapis(dodajDoRan, napisy[54] ,"35","brown", "Arial");

    PangoAttrList *atrybuty = (PangoAttrList *)pango_attr_list_new ();
	pango_attr_list_insert(atrybuty, pango_attr_size_new(28*PANGO_SCALE));
    nowe->wpisz_nazwe = (GtkEntry *)gtk_entry_new();
    gtk_entry_set_activates_default(nowe->wpisz_nazwe, TRUE);
    gtk_entry_set_max_length(nowe->wpisz_nazwe, 32);
    gtk_entry_set_text (nowe->wpisz_nazwe,napisy[55]);
    gtk_entry_set_alignment(nowe->wpisz_nazwe, 0.5);
    gtk_entry_set_attributes(nowe->wpisz_nazwe, atrybuty);

    GtkButton * dodajP = (GtkButton*)gtk_button_new();
    gtk_container_add(GTK_CONTAINER(dodajP), GTK_WIDGET(dodajDoRan));
    g_signal_connect (G_OBJECT (dodajP), "clicked", G_CALLBACK (zmknijDodajDoRank), nowe);

    GtkBox * dodawanie = (GtkBox *)gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_pack_start(dodawanie, GTK_WIDGET (nowe->wpisz_nazwe), TRUE, TRUE, 80);
    gtk_box_pack_start(dodawanie, GTK_WIDGET (dodajP), FALSE, TRUE, 80);

    GtkBox * wszystko = (GtkBox *)gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_pack_start(wszystko, GTK_WIDGET (brawo), TRUE, TRUE, 10);
    gtk_box_pack_start(wszystko, GTK_WIDGET (dostal), TRUE, TRUE, 0);
    gtk_box_pack_start(wszystko, GTK_WIDGET (podaj), TRUE, TRUE, 20);
    gtk_box_pack_start(wszystko, GTK_WIDGET (dodawanie), TRUE, TRUE, 20);

    gtk_container_add (GTK_CONTAINER (nowe->oknoDoRank), GTK_WIDGET (wszystko));
    gtk_widget_show_all (GTK_WIDGET(nowe->oknoDoRank));
    gtk_widget_grab_focus (GTK_WIDGET (nowe->oknoDoRank));
}
