#include "funkcje.h"

void wypiszBlad(void *widzet, GError* blad)
{
   if (!widzet)
   {
      g_print("Error: %s\n", blad->message);
      g_error_free (blad);
   }
}

void ustawNapis(GtkLabel * Etykieta, char tekst[], char rozmiar[], char kolor[], char czcionka[])
{

    char napis[200] = "<span font_desc=\"";
    strcat(napis, rozmiar);
    strcat(napis, ".0\" foreground=\"");
    strcat(napis, kolor);
    strcat(napis, "\" font-family = \"");
    strcat(napis, czcionka);
    strcat(napis, "\">");
    strcat(napis, tekst);
    strcat(napis, "</span>");

    gtk_label_set_markup(Etykieta, napis);

}

int losuj(int min, int max)
{
    if (max>=min)
        max-= min;
    else
    {
        int temp = min - max;
        min = max;
        max = temp;
    }
    max++;

    return max ? (rand() % max + min) : min;
}

char* int2str(int x)
{
    if(x==0)
        return "0";

    char *wynik = malloc(sizeof(char)*20);

    int roz = log10(x);
    wynik[roz+1] = '\0';

    for(int i=roz; i>=0; i--)
    {
        wynik[i]=x%10+'0';
        x/=10;
    }

return wynik;
}

void dodajRozszerzenie(char * nazwa)
{
    char *kropa=strrchr(nazwa,'.');
    char rozszerzenie[10] = ".kulki";

    gboolean nie = FALSE;

    if(kropa != NULL)
    {
        int gdzie = kropa - nazwa;
        if(strlen(nazwa)- gdzie == strlen(rozszerzenie))
        {
            for(int i=0; i<6; i++)
                if(nazwa[i+gdzie] != rozszerzenie[i])
                {
                    nie = TRUE;
                    break;
                }
        }
        if(nie == FALSE)
            return;
    }

    strcat(nazwa,".kulki");

}

gboolean naciskN(GtkWidget *event_box, GdkEventButton *wydarzenie, gpointer data)
{
    (void) event_box;
    (void) wydarzenie;

    if(klik != 0)
        klik --;
    else
    {
        GtkLabel * napis = (GtkLabel *) data;

        if(strcmp(gtk_label_get_text(napis) ,napisy[KULKI]) == 0)
            ustawNapis(napis, "MS-DOS", "120", "red", "Bauhaus 93");
        else if(strcmp(gtk_label_get_text(napis) ,"MS-DOS") == 0)
            ustawNapis(napis, "Bozydar\n 273023", "90", "#339900", "Bauhaus 93");
        else
            ustawNapis(napis, napisy[KULKI], "120", "blue", "Bauhaus 93");
    }
   return TRUE;
}

gboolean pozaPopUpem (GtkWidget *widget, GdkEventFocus *wydarzenie, gpointer dane)
{
    (void) wydarzenie;
    if(GTK_IS_WIDGET(dane))
        gtk_widget_destroy (dane);
    gtk_widget_destroy (widget);
return TRUE;
}

gboolean rozpocznj (GtkWidget *widget, GdkEventFocus *wydarzenie, gpointer dane)
{
    (void) wydarzenie;
    GtkProgressBar * pasek = dane;
    if(gtk_progress_bar_get_fraction(pasek)==1)
    {
        gtk_widget_destroy (GTK_WIDGET(pasek));
        gtk_widget_destroy (widget);
        gtk_widget_show_all((GtkWidget*)okno);
    }
return TRUE;
}
