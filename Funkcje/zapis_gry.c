#include "zapis_gry.h"

int zapiszGre(char *sciezka)
{

printf("Zapis.");
    FILE * zapis = fopen(sciezka, "w");
    if(zapis == NULL)
        return 1;
    int los = losuj(0,9);
    fprintf(zapis, "%d %d %d %d\n", plansza->wynik, (plansza->wynik)%liczbyPierwsze[los], los, plansza->kolorow);

    for(int i=0; i<9; i++)
    {
        int suma = 0;
        for(int j=0; j<9; j++)
        {
            suma+=plansza->pola[i][j]->kolor;
            fprintf(zapis, "%d ", plansza->pola[i][j]->kolor);
        }
        fprintf(zapis, "%d\n", suma%(plansza->kolorow+1));
    }

    fclose(zapis);
printf("OK\n");

return 0;
}

int wczytajGre(char *sciezka)
{
printf("Wczytaj.");
    FILE * wczyt = fopen(sciezka, "r");
    if(wczyt == NULL)
        return 1;

    int wynik, sumaK, los, kolorow;
    fscanf(wczyt, "%d %d %d %d", &wynik, &sumaK, &los, &kolorow);
    poziom = kolorow;
    plansza = inicjujPlansze(kolorow, ILE_NA_START);
    if(wynik%liczbyPierwsze[los] == sumaK)
        plansza->wynik = wynik;
    else
        plansza->wynik = 1;

    for(int i=0; i<9; i++)
    {
        int suma = 0, spr;
        for(int j=0; j<9; j++)
        {
            fscanf(wczyt, "%d", &plansza->pola[i][j]->kolor);
            suma+=plansza->pola[i][j]->kolor;
        }
        fscanf(wczyt, "%d", &spr);
        if(spr != suma%(plansza->kolorow+1))
            return 2;
    }
    fclose(wczyt);
    wypiszPlansze(plansza);

    if(wynik%liczbyPierwsze[los] != sumaK)
        return 3;
printf("OK\n");
return 0;
}
