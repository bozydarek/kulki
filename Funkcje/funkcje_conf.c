#include "funkcje_conf.h"


bool wczytajConf()
{
    FILE * conf = fopen ("config","r");

    if (!conf)
    {
        pokaz_blad("Nie znaleziono pliku konfiguracyjnego!\n Uruchamiam program z ustawieniami podstawowymi\n Aby je zmienić wejdź w \"Opcje\".\n\n To change settings and language go to \"Opcje\".\n");

        domyslnaConf();
        return false;
    }

    fscanf(conf,"%10s", jezyk);

    wczytajZmiennaBool(conf, &wskazowki);
    wczytajZmiennaBool(conf, &pelenEkran);
    printf("Tips %d\nFScr %d\n", wskazowki, pelenEkran);

    fscanf(conf,"%d", &poziom);
    printf("Poziom %d\n",poziom);

    fscanf(conf,"%d", &szybkosc);
    printf("Szybkosc kulki %d\n",szybkosc);

    if(szybkosc<1 || szybkosc>3 || poziom<1 || poziom>8)
    {
        pokaz_blad("Plik konfiguracyjny jest uszkodzony! Uruchamiam program z ustawieniami podstawowymi\n Aby je zmienić wejdź w \"Opcje\".\n\n To change settings and language go to \"Opcje\".\n");
        domyslnaConf();
        return false;
    }

    fclose(conf);

    wczytajNapisy();
    puts("Wczytano napisy.");

    if(wczytajRanking())    puts("Wczytano ranking.");
    else                    puts("Podczas wczytywania rankingu pojawily sie problemy.");


return true;
}

void wczytajZmiennaBool(FILE * plik, bool *zmienna)
{
    int temp;
    fscanf(plik, "%d", &temp);

    if(temp==0) (*zmienna) = false;
    else        (*zmienna) = true;
}

void domyslnaConf()
{
    strcpy(jezyk, "polski");
    wskazowki = pelenEkran = true;
    poziom = 5;
    szybkosc = 1;
    wczytajNapisy();
    zapiszConf();
}
void zapiszConf()
{
    FILE *conf = fopen ("config","w");

    fprintf(conf, "%s\n", jezyk);
    fprintf(conf, "%d\n", wskazowki);
    fprintf(conf, "%d\n", pelenEkran);
    fprintf(conf, "%d\n", poziom);
    fprintf(conf, "%d\n", szybkosc);
    fclose(conf);
}

void wczytajNapisy()
{
    printf("Lang: %s\n", jezyk);

    char sciezka[20] = {"./lang/"};
    strcat(sciezka, jezyk);

    FILE *lang = fopen(sciezka, "r");

    if (!lang)
    {
        pokaz_blad("Brak wybranego pliku językowego!\n Sprawdź czy wybrany plik znajduje się\n w folderze \"lang\" lub skontaktuj się z wydawcą\n");
        exit(1);
    }

    int wiersz = 0;
    for(; wiersz<ILOSC_NAPISOW; wiersz++)
    {
        char znak = getc(lang);
        int kolumna = 0;
        while(znak!='\n' && znak!=EOF)
        {
            if(znak == '\\')
            {
                znak = getc(lang);
                if(znak == 'n')
                    znak = '\n';
            }
            napisy[wiersz][kolumna++] = znak;

            znak = getc(lang);
        }
        if(znak==EOF)
            break;
    }
    fclose(lang);

    if(wiersz!=ILOSC_NAPISOW-1)
    {
        pokaz_blad("Wybrany plik językowy jest uszkodzony!\n Sprawdź zawartość pliku\n w folderze \"lang\" lub skontaktuj się z wydawcą\n");
    }
}

void zapiszRanking()
{
    FILE *rank = fopen ("rank","w+");

    for(int i=0; i<8; i++)
    {
        int suma = 0, los = losuj(1,8);
        fprintf(rank, "%d %d\n", i+1, los);
        for(int j=0; j<10; j++)
        {
            if(tablicaWynikow[i][j] == NULL) break;

            fprintf(rank, "#%s# %d\n", tablicaWynikow[i][j]->nazwa, tablicaWynikow[i][j]->wynik);
            suma+=tablicaWynikow[i][j]->wynik;
        }
        fprintf(rank,"%d\n", suma%((i+1)*liczbyPierwsze[los]));
    }
    fclose(rank);
}

bool wczytajRanking()
{
    inicjujTablice();
    FILE *rank = fopen ("rank","r");
    if(!rank)
    {
        ustawRakningDomysny();
        return false;
    }
    for(int i=0; i<8; i++)
    {
        int los, suma = 0, sumaK, nic, j=0, znak, wynik;
        fscanf(rank, "%d %d", &nic, &los);

        while(true)
        {
            znak = getc(rank);
            if(znak == '\n') znak = getc(rank);
            if(znak == '#')
            {
                znak = getc(rank);
                char nazwa[50];
                int poz = 0;
                while(znak != '#')
                {
                    nazwa[poz] = znak;
                    znak = getc(rank);
                    poz++;
                }
                nazwa[poz] = '\0';
                fscanf(rank, "%d\n", &wynik);

                dodajGracza(nazwa,wynik,i+1,false);
                suma+=wynik;
                j++;
            }
            else
            {
                ungetc(znak, rank);
                break;
            }

        }

        fscanf(rank, "%d", &sumaK);

        if(sumaK != suma%(nic*liczbyPierwsze[los]))
        {
            pokaz_blad(napisy[OSZUKANYRANKING]);
            return false;
        }
    }
    fclose(rank);

    return true;
}

void ustawRakningDomysny()
{
    char ja[20] = "Bożydar";
    for(int i=0; i<8;i++)
        for (int j=0; j<9; j++)
        {
            dodajGracza(ja, 3000-(j+i)*150, i+1, false);
        }
}
