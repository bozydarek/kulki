#ifndef RANKING_H
#define RANKING_H

#include <gtk/gtk.h>
#include <stdlib.h>

typedef struct ranki
{
    GtkLabel *etykietaGracza;
    GtkLabel *numer;
    GtkLabel *wynik;
}ranki;

typedef ranki** ranking;

ranking inicjujRanking(int ilosc);
///-------------------------------
typedef struct oknoRankingowe
{
    GtkWindow * oknoPodSpodem;
    GtkWindow * oknoDoRank;
    GtkEntry * wpisz_nazwe;

}oknoRankingowe;

#endif // RANKING_H
