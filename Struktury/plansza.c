#include "plansza.h"

Pole *inicjujPole(int X, int Y)
{
    Pole *nowe;

    if((nowe=malloc(sizeof(Pole)))==NULL)
        exit(102);

    nowe->x = X;
    nowe->y = Y;
    nowe->kolor = 0;

    return nowe;
}
///------------------------------------------
Plansza inicjujPlansze(int ileKolorow, int ileNaStart)
{
    Plansza nowa;

    if((nowa=malloc(sizeof(calosc)))==NULL)
        exit(101);

    for(int i=0; i<9; i++)
        for(int j=0; j<9; j++)
            nowa->pola[i][j]=inicjujPole(i,j);

    nowa->wynik = 0;
    nowa->kolorow = ileKolorow;
    nowa->aktywnePole = NULL;
    nowa->TrwaPrzemieszczenie = false;
    nowa->trasa = NULL;
    nowa->TrwaGra = true;
    losujkulki(nowa, ileNaStart);

    return nowa;
}

void losujkulki(Plansza plansza,int ile)
{
    for(int i=0; i<ile;)
    {
        if(ustawKulkeLosowo(plansza,losuj(1,plansza->kolorow)) == true)
            i++;
    }
    for(int i=0; i<3; i++)
        plansza->nowe[i] = losuj(1,plansza->kolorow);
}

bool ustawKulkeLosowo(Plansza plansza, int kolor)
{
    int x = losuj(0,8), y = losuj(0,8);

    if(plansza->pola[x][y]->kolor==0)
    {
        ustawKulke(plansza,x,y,kolor);
        return true;
    }
    return false;
}

void ustawKulke(Plansza plansza,int x, int y, int kolor)
{
    plansza->pola[x][y]->kolor=kolor;
}

int ileWolnychMiejsc(Plansza plansza)
{
    int ile = 0;
    for(int i=0; i<9; i++)
        for(int j=0; j<9; j++)
            if(plansza->pola[i][j]->kolor == 0)
                ile++;
    return ile;
}
///-------------- DEBUG ---------------
void wypiszPlansze(Plansza plansza)
{
    puts("--- DEBUG PLANSZY ---");
    for(int i=0; i<9; i++)
    {
        for(int j=0; j<9; j++)
            printf(" %d", plansza->pola[i][j]->kolor);
        puts("");
    }
    puts("--- ----- ------ ---");
}
