#include "kolejka.h"

wenzel *inicjujWenzel(int liczba)
{
    wenzel * w = malloc(sizeof(wenzel));
    w->nast = NULL;
    w->wartosc = liczba;
    return w;
}
///------------------------
kolejka inicjuj()
{
    kolejka q = malloc(sizeof(kolej));

    q->iloscElementow = 0;
    q->koniec = q->pocz = NULL;

    return q;
}
void niszcz(kolejka wsk)
{
    if(wsk!=NULL)
    {
        wyczysc(wsk);
        free(wsk);
    }
}

void wyczysc(kolejka wsk)
{
    for(int i=0; i<wsk->iloscElementow; i++)
        pobierz(wsk);

    wsk->iloscElementow = 0;
    wsk->koniec = wsk->pocz = NULL;
}
void dodaj(kolejka wsk, int liczba)
{
    wenzel *nowe = inicjujWenzel(liczba);
    if(wsk->iloscElementow == 0)
    {
        wsk->pocz = nowe;
        wsk->koniec = nowe;
    }
    else
    {
        wsk->koniec->nast = nowe;
        wsk->koniec = nowe;
    }

    wsk->iloscElementow++;
}

void dodajNaPoczatek(kolejka wsk, int liczba)
{
    wenzel *nowe = inicjujWenzel(liczba);
    if(wsk->iloscElementow == 0)
    {
        wsk->pocz = nowe;
        wsk->koniec = nowe;
    }
    else
    {
        nowe->nast = wsk->pocz;
        wsk->pocz = nowe;
    }

    wsk->iloscElementow++;
}

int pobierz(kolejka wsk)
{
    if(czyPusta(wsk))
    {
        return -1;
    }

    wenzel *temp = wsk->pocz;
    int wart = temp->wartosc;
    free(temp);

    wsk->pocz = wsk->pocz->nast;

    wsk->iloscElementow--;

return wart;
}
bool czyPusta(kolejka wsk)
{
    return wsk->iloscElementow == 0;
}
