#include "gracz.h"

void inicjujTablice()
{
    for(int i=0; i<8; i++)
        for(int j=0; j<10; j++)
        {
            tablicaWynikow[i][j] = NULL;
        }
}
static void przesunWTabeli(int poziom, int pozycja)
{
    for(int i=9; i>pozycja; i--)
        tablicaWynikow[poziom-1][i] = tablicaWynikow[poziom-1][i-1];
}
bool czyDodwacDoWynikow(int poziom, int wynik)
{
    for(int i=0; i<10; i++)
        if(tablicaWynikow[poziom-1][i] == NULL)
        {
            return true;
        }
        else if(tablicaWynikow[poziom-1][i]->wynik < wynik)
        {
            return true;
        }
    return false;
}

static bool dodajDoWynikow(gracz *nowy)
{
    for(int i=0; i<10; i++)
        if(tablicaWynikow[nowy->poziom-1][i] == NULL)
        {
            tablicaWynikow[nowy->poziom-1][i] = nowy;
            return true;
        }
        else if(tablicaWynikow[nowy->poziom-1][i]->wynik < nowy->wynik)
        {
            przesunWTabeli(nowy->poziom, i);
            tablicaWynikow[nowy->poziom-1][i] = nowy;
            return true;
        }

    free(nowy); /// nowy gracz nie jest nam już potrzebny
    return false;
}

bool dodajGracza(char *nazwa, int wynik, int poziom, bool tajne)
{
    gracz *nowy;
    nowy = malloc(sizeof(gracz));
    nowy->nazwa = malloc(sizeof(char)*40);

    strcpy(nowy->nazwa, nazwa);
    nowy->wynik = wynik;
    nowy->poziom = poziom;
    nowy->tajemnica = tajne;

    return dodajDoWynikow(nowy);
}
