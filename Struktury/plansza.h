#ifndef PLANSZA_H
#define PLANSZA_H

#include <stdbool.h>

#include "../Funkcje/funkcje.h"
#include "../Struktury/kolejka.h"

///-----------------------------------
typedef struct Pole{
    int x, y;
    int kolor;
}Pole;

Pole *inicjujPole(int X, int Y);
///-----------------------------------
typedef struct calosc{

    Pole *pola[9][9];
    Pole *aktywnePole;
    /*struct aktywnePole{
        int x, y;
    }aktywnePole;*/
    int nowe[3];

    int kolorow;

    int wynik;

    bool TrwaPrzemieszczenie;
    bool TrwaGra;

    int dlDrogi;
    kolejka trasa;
}calosc;

typedef calosc* Plansza;

Plansza inicjujPlansze(int ileKolorow, int ileNaStart);
void losujkulki(Plansza plansza,int ile);
bool ustawKulkeLosowo(Plansza plansza, int kolor);
void ustawKulke(Plansza plansza,int x, int y, int kolor);
int ileWolnychMiejsc(Plansza plansza);
///-------------- DEBUG ---------------
void wypiszPlansze(Plansza plansza);

#endif // PLANSZA_H
