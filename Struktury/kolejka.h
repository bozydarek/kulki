#ifndef KOLEJKA_H
#define KOLEJKA_H

#include <stdlib.h>
#include <stdbool.h>

typedef struct wenzel
{
    int wartosc;
    struct wenzel* nast;

}wenzel;

wenzel * inicjujWenzel(int liczba);

typedef struct kolej {

    int iloscElementow;
    wenzel *pocz, *koniec;

}kolej;

typedef kolej* kolejka;

kolejka inicjuj();
void niszcz(kolejka wsk);

void wyczysc(kolejka wsk);
void dodaj(kolejka wsk, int liczba);
void dodajNaPoczatek(kolejka wsk, int liczba);
int pobierz(kolejka wsk);
bool czyPusta(kolejka wsk);

#endif // KOLEJKA_H
