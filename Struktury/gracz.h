#ifndef GRACZ_H
#define GRACZ_H

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct gracz{

    char *nazwa;
    int wynik;
    int poziom;
    bool tajemnica;

}gracz;

gracz *tablicaWynikow[8][10];

void inicjujTablice();
bool czyDodwacDoWynikow(int poziom, int wynik);
bool dodajGracza(char *nazwa, int wynik, int poziom, bool tajne);


#endif // GRACZ_H
